package main

import (
  "encoding/json"
  "fmt"
  "log"
  "os/exec"
  "strings"
)

type Game struct {
  Name string       `json:"name"`
  Size int          `json:"size"`
  Board [][]int     `json:"board"`
  OnePieces int     `json:"onePieces"`
  TwoPieces int     `json:"twoPieces"`
}
func main() {
  size := 9
  uuid, err := exec.Command("uuidgen").Output()
  if err != nil {
    log.Fatal(err)
  }
  game := Game{strings.TrimRight(string(uuid),"\n"), size, initializeBoard(size), 50, 50}
  gameJson, err := json.Marshal(game)
  if err != nil {
    log.Fatal(err)
  }
  fmt.Println(string(gameJson))
}

func initializeBoard(size int)[][]int {
  temp := make([][]int, size)
  for row := range temp {
    temp[row] = make([]int, size)
  }
  return temp
}

